// using System.Text;
// using Microsoft.Extensions.DependencyInjection;
// using MyVod.Domain.SharedKernel;
//
// namespace GeoFindService;
//
// public interface IGeoFindService
// { 
//     RegionIdentifier GetUserLocation();
// }
//
// public class GeoFindService : IGeoFindService
// {
//     public RegionIdentifier GetUserLocation()
//     {
//         return RegionIdentifier.Poland;
//     }
// }
//
//
// public static class Extensions 
// {
//     public static IServiceCollection AddGeoFindService(this IServiceCollection services,
//         Action<GeoFindConfiguration> conf)
//     {
//         var config = new GeoFindConfiguration();
//
//         conf(config);
//         
//         services.AddTransient<IGeoFindService, GeoFindService>();
//
//         return services;
//     }
// }
//
// public class GeoFindConfiguration
// {
//     public string ApiKey;
//     public GeoFindMethod Method;
// };
//
// public enum GeoFindMethod
// {
//     Ip,
//     Cdn
// }
