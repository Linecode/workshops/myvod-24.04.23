using Stripe.Checkout;

namespace StripeClient;

public class CheckoutSession
{
    private readonly StripeConfiguration _stripeConfiguration;

    public CheckoutSession(StripeConfiguration stripeConfiguration)
    {
        _stripeConfiguration = stripeConfiguration;
    }

    public async Task<CreateCheckoutSessionResponse> CreateCustomCheckoutSessionResponse(CreateCustomCheckoutSessionRequest request)
    {
        var options = new SessionCreateOptions
        {
            LineItems = new List<SessionLineItemOptions>
            {
                new()
                {
                    PriceData = new SessionLineItemPriceDataOptions
                    {
                        UnitAmount = request.Price,
                        Currency = request.Currency,
                        ProductData = new SessionLineItemPriceDataProductDataOptions
                        {
                            Name = request.Name,
                        },
                    },
                    Quantity = request.Quantity,
                },
            },
            Mode = "payment",
            SuccessUrl = _stripeConfiguration.SuccessUrl,
            CancelUrl = _stripeConfiguration.ErrorUrl,
            ClientReferenceId = request.OrderId.ToString()
        };

        var service = new SessionService();
        var session = await service.CreateAsync(options);
        
        var response = new CreateCheckoutSessionResponse()
        {
            SessionId = session.Id,
            RedirectUrl = session.Url
        };
        
        return response;
    }
}

public class CreateCheckoutSessionResponse
{
    public string RedirectUrl { get; init; }

    public string SessionId { get; init; }
}

public class CreateCustomCheckoutSessionRequest
{
    public string Name { get; init; }
    
    public long Price { get; init; }

    public string Currency { get; init; }
    
    public int Quantity { get; init; }
    public Guid OrderId { get; set; }
}