using Microsoft.Extensions.DependencyInjection;

namespace StripeClient;

public class StripeConfiguration
{
    public string SuccessUrl { get; set; }
    public string ErrorUrl { get; set; }
    public string ApiKey { get; set; }
}

public static class ServiceExtension
{
    public static IServiceCollection AddStripe(this IServiceCollection services, Action<StripeConfiguration> conf)
    {
        var config = new StripeConfiguration();
        
        conf?.Invoke(config);

        Stripe.StripeConfiguration.ApiKey = config.ApiKey;

        services.AddSingleton(_ => config);

        services.AddTransient<CheckoutSession>();
        // services.AddTransient<IPaymentService, StripePaymentService>();

        return services;
    }
}