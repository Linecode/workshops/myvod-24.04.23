using System.Text.Json.Serialization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyVod.Domain.Reporting.Application;
using MyVod.Domain.Reporting.Domain;

namespace MyVod.Reporting;

[ApiController]
public class ReportingController : Controller
{
    private readonly ISender _sender;

    public ReportingController(ISender sender)
    {
        _sender = sender;
    }

    [HttpPost("reports-set")]
    public async Task<IActionResult> CreateReportsSet(CancellationToken ct = default)
    {
        var result = await _sender.Send(new CreateReportSetCommand(), ct);

        return result.Match<IActionResult>(
            success: r => Created(new Uri($"reports-set/{r}", UriKind.Relative), null),
            error: e => BadRequest(e.Message)
        );
    }

    [HttpPost("reports-set/{id}")]
    public async Task<IActionResult> AddReportToReportSet(Guid id, [FromBody] ReportDto reportDto, CancellationToken ct = default)
    {
        var result = await _sender.Send(new AddReportCommand(id, reportDto.ReportType));
        
        return result.Match<IActionResult>(
            success: Ok,
            error: e => BadRequest(e.Message)
        );
    }
}

public record ReportDto(
    [property:JsonRequired]ReportsSet.ReportType ReportType
    );