using Microsoft.AspNetCore.Mvc;
using MyVod.Services;

namespace MyVod.Controllers;

public class MoviesController : Controller
{
    private readonly IMovieService _movieService;

    public MoviesController(IMovieService movieService)
    {
        _movieService = movieService;
    }

    [HttpGet("[controller]/{id:guid}")]
    [ActionName("")]
    public async Task<IActionResult> Index(Guid id)
    {
        var movie = await _movieService.Get(id);
        return View("Index", movie);
    }
}
