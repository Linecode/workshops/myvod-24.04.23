using System.Reflection;
// using GeoFindService;
// using GolabexClient;
using Marten;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MyVod.Data;
using MyVod.Domain.Common;
using MyVod.Domain.Reporting;
using MyVod.Infrastructure;
using MyVod.Services;
using StripeClient;
using Weasel.Core;
using OrderService = MyVod.Services.OrderService;
using PersonService = MyVod.Services.PersonService;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddMarten(_ =>
{
    _.UseDefaultSerialization(nonPublicMembersStorage: NonPublicMembersStorage.NonPublicSetters);
    _.Connection(builder.Configuration.GetConnectionString("Marten"));
    _.AutoCreateSchemaObjects = AutoCreate.All;
});

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString));
builder.Services.AddDbContext<MoviesDbContext>(options =>
{
    options.UseSqlite(connectionString, 
        x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
});
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
// builder.Services.AddGolabex(_ =>
// {
//     _.ApiKey = "3C1951A3-F1A0-4C5D-B020-A1F92B8DCFAE";
//     _.ApiSecret = "E0CD9469-3ACB-4B12-8047-271FE7B38DD9";
// });
// builder.Services.AddGeoFindService(_ =>
// {
//     _.ApiKey = "4199380D-04AA-4C7D-B40F-9EBCAF67F76D";
//     _.Method = GeoFindMethod.Cdn;
// });

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddControllersWithViews()
    .AddRazorRuntimeCompilation();

builder.Services.AddStripe(_ =>
{
    _.ApiKey =
        "sk_test_51L3x0jIP4aoyQ2vi0RgDKWCPmI22ugYLYJbCeUBrgfYMEE0HBegyc5sJDt06xeEc1PQVtXqv56by02gLDwXZyU8Q00QT28pWGf";
    _.ErrorUrl = "https://localhost:7220/order/process/error/";
    _.SuccessUrl = "https://localhost:7220/order/process/success/";
});

builder.Services.AddScoped<IMovieService, MovieService>();
builder.Services.AddScoped<IGenreService, GenreService>();
builder.Services.AddScoped<IPersonService, PersonService>();
builder.Services.AddScoped<IOrderService, OrderService>();

builder.Services.AddCommonModule();
builder.Services.AddReportingModule();

builder.Services.AddMediatR(cnf => cnf.RegisterServicesFromAssembly(typeof(ReportingModule).Assembly));

// Mongo settings

builder.Services.AddSingleton<IMongoClient>(new MongoClient("mongodb://docker:mongopw@localhost:55000"));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "MyArea",
    pattern: "{area:exists}/{controller=Dashboard}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();
