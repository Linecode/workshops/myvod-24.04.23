using Microsoft.Extensions.DependencyInjection;

namespace MyVod.Domain.Common;

public static class CommonModule
{
    public static IServiceCollection AddCommonModule(this IServiceCollection services)
    {
        services.AddTransient<IClock, Clock>();
        
        return services;
    }
}