using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Domain.Common;
using MyVod.Domain.Reporting.Domain;
using MyVod.Domain.Reporting.Domain.Policies.Create;
using MyVod.Domain.Reporting.Infrastructure;

namespace MyVod.Domain.Reporting.Application;

public record CreateReportSetCommand : IRequest<Result<Guid>>;

public class CreateReportSetHandler : IRequestHandler<CreateReportSetCommand, Result<Guid>>
{
    private readonly IRepository _repository;
    private readonly IClock _clock;

    public CreateReportSetHandler(IRepository repository, IClock clock)
    {
        _repository = repository;
        _clock = clock;
    }

    public async Task<Result<Guid>> Handle(CreateReportSetCommand request, CancellationToken cancellationToken)
    {
        // CompositeCreatePolicy jest publiczne co umożliwia dynamiczne tworzenie polityk,
        // jezeli taka funkcjonalnosc nie jest pozadana to mozemy to api schowac za pomoca klasy prywatnej 
        // i udostepniac predefiniowane polityki za pomoca pol statycznych
        // Linijki 25 - 31 moglibysmy przeniesc do fabryki
        var policy = new CreatePolicies.CompositeCreatePolicy(
            new CreatePolicies.CreateOnlyAtMonthStart(_clock)
            );
        
        var result = ReportsSet.Create(policy);

        // Monady pozwalaja na ograniczenie ilosci instrukcji "if" w kodzie, przy okazji "zmuszajac" programiste do 
        // zaimplementowania sciezki obslugi bledu (inaczej kod sie nie skompiluje)
        return await result.MatchAsync<Result<Guid>>(
            success: async report =>
            {
                // ofc to powinno byc ladniej zrobione przy pomocy transakcji etc.
                await _repository.Save(report.Id, report);
                return Result<Guid>.Success(report.Id);
            },
            error: e => Result<Guid>.Error(e));
    }
}