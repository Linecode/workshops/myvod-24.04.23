using MediatR;
using MyVod.Domain.Reporting.Domain;

namespace MyVod.Domain.Reporting.Application.Listeners;

public class ReportReadyToReviewListener : INotificationHandler<ReportsSet.Events.ReportsSetReadyToBeReviewed>
{
    public async Task Handle(ReportsSet.Events.ReportsSetReadyToBeReviewed notification, CancellationToken cancellationToken)
    {
        await Task.Delay(100, cancellationToken);
        Console.WriteLine("Notification sent about Reports set ready to be reviewed.");
    }
}