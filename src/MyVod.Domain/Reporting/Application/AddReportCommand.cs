using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Domain.Reporting.Domain;
using MyVod.Domain.Reporting.Infrastructure;

namespace MyVod.Domain.Reporting.Application;

public record AddReportCommand(Guid ReportsSetId, ReportsSet.ReportType ReportType/*, dane do stworzenia raporty */) : IRequest<Result>;

public class AddReportCommandHandler : IRequestHandler<AddReportCommand, Result>
{
    private readonly IRepository _repository;
    private readonly IPublisher _publisher;

    public AddReportCommandHandler(IRepository repository, IPublisher publisher)
    {
        _repository = repository;
        _publisher = publisher;
    }
    
    public async Task<Result> Handle(AddReportCommand request, CancellationToken cancellationToken)
    {
        var reportsSet = await _repository.Get<ReportsSet>(request.ReportsSetId);

        if (reportsSet is null) return Result.Error(new Exception("Not found"));

        var result = reportsSet.AddReport(new Report(DateTimeOffset.UtcNow, "author"), request.ReportType);

        return await result.MatchAsync(
            success: async events =>
            {
                await _repository.Save(reportsSet.Id, reportsSet);

                foreach (var @event in events)
                {
                    await _publisher.Publish(@event, cancellationToken);
                }

                return Result.Success();
            },
            error: e => Result.Error(e) 
        );
    }
}