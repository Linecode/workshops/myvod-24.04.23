using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Reporting.Domain.Policies.Create;

namespace MyVod.Domain.Reporting.Domain;


// 1. Genrowany po stronie bazy danych (inkrementowalne id)
// 2. Generowany po stronie klienta (Guid, uuid, ulid)
// 3. Sekwencje ( HiLo )
public sealed class ReportsSet : Entity<Guid>, AggregateRoot<Guid>
{
    public ReportsSetStates State { get; set; } = ReportsSetStates.Created;
    
    private readonly Dictionary<ReportType, Report> _reports = new();
    public IReadOnlyDictionary<ReportType, Report> Reports => _reports.AsReadOnly();

    private ReportsSet() {}

    public static Result<ReportsSet> Create(CreatePolicy policy)
    {
        var report = new ReportsSet { Id = Guid.NewGuid() };

        var result = policy.Create(report);

        return result.Match(
            success: () => Result<ReportsSet>.Success(report),
            error: Result<ReportsSet>.Error
        );
    }

    public Result<IEnumerable<IDomainEvent>> AddReport(Report report, ReportType type)
    {
        _reports[type] = report;
    
        // Invariant
        if (IsReportsSetReadyToReview())
        {
            State = ReportsSetStates.Ready;
    
            return Result<IEnumerable<IDomainEvent>>.Success(new[] { new Events.ReportsSetReadyToBeReviewed { ReportsSetId = Id } });
        }

        return Result<IEnumerable<IDomainEvent>>.Success(Array.Empty<IDomainEvent>());
    }

    private bool IsReportsSetReadyToReview()
    {
        var hasFinanceReport = _reports.TryGetValue(ReportType.Finance, out _);
        var hasSalesReport = _reports.TryGetValue(ReportType.Sales, out _);
        return hasFinanceReport && hasSalesReport;
    }
    
    public enum ReportsSetStates
    {
        Created,
        Ready,
        Positive,
        Decline
    }

    public enum ReportType
    {
        Finance,
        Sales,
    }

    public static class Events
    {
        public class ReportsSetReadyToBeReviewed : IDomainEvent
        {
            public Guid ReportsSetId { get; init; }
        }
    } 
}