using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Reporting.Domain;

public class Report : Entity<Guid>
{
    public DateTimeOffset CreateAt { get; private set; }
    public string Author { get; private set; }

    private readonly List<object> _data = new();
    public IReadOnlyList<object> Data => _data.AsReadOnly();

    public Report(DateTimeOffset createAt, string author)
    {
        CreateAt = createAt;
        Author = author;
    }
    
    public void AddData(object data)
    {
        _data.Add(data);
    }
}