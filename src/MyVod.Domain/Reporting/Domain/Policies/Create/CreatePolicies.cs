using MyVod.Common.BuildingBlocks.Common;
using MyVod.Domain.Common;

namespace MyVod.Domain.Reporting.Domain.Policies.Create;

// Jakaś przykładowa polityka która określa ramy czasowe kiedy zbiór raportów moze zostac utworzony
public static class CreatePolicies
{
    public class CreateOnlyAtMonthStart : CreatePolicy
    {
        private readonly IClock _clock;

        public CreateOnlyAtMonthStart(IClock clock)
        {
            _clock = clock;
        }
        
        public Result Create(ReportsSet _)
        {
            return _clock.UtcNow.Day < 2 ? Result.Success() : Result.Error(new Exception("Can't create reports after the start of month"));
        }
    }

    // Polityka do wykorzsytania w testach (moglaby zostac zadeklarowana w testach) poniewaz polityka wyzej bedzie
    // powodowac blad przez wiekszosc czasu
    public class AlwaysSuccessPolicy : CreatePolicy
    {
        public Result Create(ReportsSet reportsSet)
            => Result.Success();
    }

    public class CompositeCreatePolicy : CreatePolicy
    {
        private readonly CreatePolicy[] _createPolicies;

        public CompositeCreatePolicy(params CreatePolicy[] createPolicies)
        {
            _createPolicies = createPolicies;
        }

        public Result Create(ReportsSet reportsSet)
            => _createPolicies.Select(x => x.Create(reportsSet))
                .Where(x => !x.IsSuccessful)
                .DefaultIfEmpty(Result.Success())
                .First();
    }
}