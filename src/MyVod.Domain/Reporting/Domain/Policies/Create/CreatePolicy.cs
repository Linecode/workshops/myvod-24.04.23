using MyVod.Common.BuildingBlocks.Common;

namespace MyVod.Domain.Reporting.Domain.Policies.Create;

// ReSharper disable once InconsistentNaming
public interface CreatePolicy
{
    Result Create(ReportsSet reportsSet);
}
