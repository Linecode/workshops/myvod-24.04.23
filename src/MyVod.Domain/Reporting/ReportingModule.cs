using Microsoft.Extensions.DependencyInjection;
using MyVod.Domain.Reporting.Infrastructure;

namespace MyVod.Domain.Reporting;

public static class ReportingModule
{
    public static IServiceCollection AddReportingModule(this IServiceCollection services)
    {
        services.AddSingleton<IRepository, InMemoryRepository>();
        
        return services;
    }
}