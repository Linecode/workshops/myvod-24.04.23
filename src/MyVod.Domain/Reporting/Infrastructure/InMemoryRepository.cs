namespace MyVod.Domain.Reporting.Infrastructure;

public interface IRepository
{
    Task Save<T>(Guid id, T entity);
    Task<T?> Get<T>(Guid id);
}

// Implementacja Mock-a tylko po to aby zasymulowac prace na bazie danych
public class InMemoryRepository : Dictionary<Guid, object>, IRepository
{
    
    // Id w api tutaj to tylko uproszczenie aby mozna bylo przyjac dowolny typ
    public Task Save<T>(Guid id, T entity)
        where T : notnull
    {
        this[id] = entity;
        return Task.CompletedTask;
    }

    public Task<T?> Get<T>(Guid id)
        where T : notnull
    {
        var found = TryGetValue(id, out var entity);
        
        if (found)
            return Task.FromResult((T)entity);
        
        return Task.FromResult(default(T));
    }
}