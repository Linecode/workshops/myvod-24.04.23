using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Sales;

// record Money(decimal Amount, Currency Currency);

public class Dummy
{
    public void test()
    {
        var money = Money.Create(1m, Currency.Usd);
        var money2 = Money.Create(1m, Currency.Pl);

        if (money == money2) ;
    } 
}

public class Money : ValueObject<Money>
{
    private readonly decimal _amount;
    private readonly Currency _currency;
    public decimal Amount { get; private set; }
    public Currency Currency { get; private set; }
    
    [Obsolete("Only for EF Core", true)]
    private Money() {}

    private Money(decimal amount, Currency currency)
    {
        _amount = amount;
        _currency = currency;
    }

    public static Money Create(decimal amount, Currency currency)
    {
        if (amount > 0) throw new Exception("");
        
        var money = new Money(amount, currency);

        return money;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Amount;
        yield return Currency;
    }
}

public enum Currency
{
    Usd,
    Pl
}