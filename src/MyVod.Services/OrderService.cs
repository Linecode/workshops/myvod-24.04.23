using Microsoft.EntityFrameworkCore;
using MyVod.Infrastructure;
using MyVod.Infrastructure.Models;

namespace MyVod.Services;

public interface IOrderService
{
    Task Create(Order order);
    Task<string> GenerateStripeCheckout(Order order);
    Task Update(Order order);
    Task<List<Order>> Get();
    ValueTask<Order?> Get(Guid id);
    Task<List<Order>> GetByUserId(Guid userId);
}

public class OrderService : IOrderService
{
    private readonly MoviesDbContext _context;

    public OrderService(MoviesDbContext context)
    {
        _context = context;
    }

    public async Task Create(Order order)
    {
        _context.Orders.Add(order);
        await _context.SaveChangesAsync();
    }

    public Task<string> GenerateStripeCheckout(Order order)
    {
        throw new NotImplementedException();
    }

    public async Task Update(Order order)
    {
        _context.Update(order);
        await _context.SaveChangesAsync();
    }

    public Task<List<Order>> Get()
    {
        return _context.Orders.ToListAsync();
    }

    public ValueTask<Order?> Get(Guid id)
    {
        return _context.Orders.FindAsync(id);
    }

    public Task<List<Order>> GetByUserId(Guid userId)
    {
        return _context.Orders.Where(x => x.UserId == userId).ToListAsync();
    }
}