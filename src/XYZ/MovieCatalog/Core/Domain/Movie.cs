using MyVod.Common.BuildingBlocks.Ddd;

namespace XYZ.MovieCatalog.Core.Domain;

public class Movie : Entity<Guid>, AggregateRoot<Guid>
{
    public string Title { get; private set; }

    public static Movie Create(string title)
    {
        var movie = new Movie
        {
            Title = title,
            Id = Guid.NewGuid()
        };
        
        return movie;
    }
    
    public enum MovieState
    {
        Created,
        Published
    }
}