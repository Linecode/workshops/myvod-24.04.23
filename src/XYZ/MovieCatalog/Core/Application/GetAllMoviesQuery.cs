using XYZ.MovieCatalog.Core.Application.Ports;

namespace XYZ.MovieCatalog.Core.Application;

public interface IQuery<TResult> {}
public interface IQueryHandler<TQuery, TResult>
    where TQuery : IQuery<TResult>
{
    public Task<TResult> Handle(TQuery query);
}

public class GetAllMoviesQuery : IQuery<object>
{
}

internal class GetAllMoviesQueryHandler : IQueryHandler<GetAllMoviesQuery, object>
{
    private readonly IRepository _repository;

    public GetAllMoviesQueryHandler(IRepository repository)
    {
        _repository = repository;
    }
    
    public Task<object> Handle(GetAllMoviesQuery query)
    {
        return Task.FromResult(_repository.GetMovies());
    }
} 