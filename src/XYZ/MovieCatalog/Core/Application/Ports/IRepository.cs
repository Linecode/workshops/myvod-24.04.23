namespace XYZ.MovieCatalog.Core.Application.Ports;

public interface IRepository
{
    object GetMovies();
}