﻿// using Microsoft.Extensions.Logging;
// using MyVod.Domain.Sales.B2b.Subscription.Application.Ports;
//
// namespace GolabexClient;
//
// public class MockGolabexAdapter : IExternalNotificationService
// {
//     private readonly ILogger<MockGolabexAdapter> _logger;
//
//     public MockGolabexAdapter(ILogger<MockGolabexAdapter> logger)
//     {
//         _logger = logger;
//     }
//     
//     public async Task<bool> SendNotification(string address, string text)
//     {
//         await Task.Delay(100);
//         
//         _logger.LogInformation("Message send to: {Address} with body: {Text}", address, text);
//
//         // just for testing
//         return true;
//     }
// }