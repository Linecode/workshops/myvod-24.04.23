// using Microsoft.Extensions.DependencyInjection;
// using MyVod.Domain.Sales.B2b.Subscription.Application.Ports;
//
// namespace GolabexClient;
//
// // This should be for nuget package for example
// public class GolabexClient
// {
//     public async Task SendParcel(Dictionary<string, object> someStupidApi)
//     {
//         await Task.Delay(500);
//     }
// }
//
// // Because all libs have some configuration extensions
// public static class ServiceExtensions
// {
//     public static IServiceCollection AddGolabex(this IServiceCollection services, Action<GolabexConfiguration> configAction)
//     {
//         var config = new GolabexConfiguration();
//
//         configAction(config);
//         
//         services.AddTransient<GolabexClient>();
//         services.AddTransient<IExternalNotificationService, MockGolabexAdapter>();
//
//         return services;
//     }
// }
//
// public class GolabexConfiguration
// {
//     public string ApiKey { get; set; }
//     public string ApiSecret { get; set; }
// }
